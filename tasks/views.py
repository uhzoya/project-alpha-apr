from django.shortcuts import render
from .models import Task
from django.contrib.auth.decorators import login_required


@login_required
def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)

    return render(
        request, "show_my_tasks.html", {"show_my_tasks": show_my_tasks}
    )
