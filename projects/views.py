from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(author=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    show_project = Project.objects.get(id=id)
    context = {
        "show_project": show_project,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(instance=request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.author = request.user
            project.save()
            return redirect("list_projects")

    form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
